package com.mappau.apps.digitaltimestampclock;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by guru on 29.10.2015.
 */
public class TimeTask {

    static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    public Date start, end;

    public TimeTask(){
        start = new Date();
    }

    public TimeTask(Date start){
        this.start = start;
    }

    public TimeTask(Date start, Date end){
        this.start = start;
        this.end = end;
    }

    public void end(){
        end =  new Date();
    }

    public String toString(){
        return sdf.format(start) +" - "+ format((end==null)?(new Date().getTime()-start.getTime()):(end.getTime()-start.getTime()));
    }

    public String format(long millis){

        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }


}
