package com.mappau.apps.digitaltimestampclock;

import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity {

    final static ArrayList<TimeTask> list = new ArrayList<TimeTask>();
    static ArrayAdapter arrayAdapter = null;
    static Timer timer = null;
    static FloatingActionButton fab = null;
    static final String backenapi = "http://tic.mappau.com";
    static final String user = "peter";
    static boolean time_running = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        updateList();

        fab = (FloatingActionButton) findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!time_running)
                {
                    start();
                }
                else
                {
                    end();

                }

                arrayAdapter.notifyDataSetChanged();
            }
        });

        ListView listview  = (ListView) findViewById(R.id.listView);

        arrayAdapter = new ArrayAdapter<TimeTask>(this,R.layout.list_item,R.id.itemText,list);



        listview.setAdapter(arrayAdapter);

        timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        arrayAdapter.notifyDataSetChanged();
                    }
                });
            }
        }, 0, 1000);



    }

    public void start(){

        AsyncTask<Void, Void, Void> task  = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                HttpURLConnection urlConnection = null;

                try {
                    URL url = new URL(backenapi+"/"+user+"/start");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestMethod("POST");
                    urlConnection.getContent();

                }catch (IOException e){
                    e.printStackTrace();

                }
                finally {
                    urlConnection.disconnect();
                }


                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                updateList();
                super.onPostExecute(aVoid);
            }
        };

        task.execute();


    }

    public void end(){

        AsyncTask<Void, Void, Void> task  = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                HttpURLConnection urlConnection = null;

                try {
                    URL url = new URL(backenapi+"/"+user+"/end");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestMethod("POST");
                    urlConnection.getContent();

                }catch (IOException e){
                    e.printStackTrace();

                }
                finally {
                    urlConnection.disconnect();
                }


                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                updateList();
                super.onPostExecute(aVoid);
            }
        };

        task.execute();


    }

    public void updateList(){
        Log.d("TIMESTAMP","wants to update List");
        AsyncTask<Void, Void, Void> task  = new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... params) {

                HttpURLConnection urlConnection = null;

                try {

                    URL url = new URL(backenapi+"/"+user+"/sessions");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setUseCaches(false);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder responseStrBuilder = new StringBuilder();

                    String inputStr;
                    while ((inputStr = reader.readLine()) != null)
                        responseStrBuilder.append(inputStr);
                    JSONObject obj = new JSONObject(responseStrBuilder.toString());

                    JSONArray sessions = obj.getJSONArray("sessions");

                    Log.d("TIMESTAMP","session lenght: "+ sessions.length());
                    if(sessions.length()==0)
                    {
                        time_running = false;

                    }
                    list.clear();
                    for(int i=0; i<sessions.length();i++)
                    {
                        JSONObject session =  sessions.getJSONObject(i);
                        TimeTask timeTask = null;
                        if(session.has("end"))
                        {
                            timeTask = new TimeTask(new Date(session.getLong("start")),new Date(session.getLong("end")));
                            time_running = false;

                        }else
                        {
                            timeTask = new TimeTask(new Date(session.getLong("start")));
                            time_running = true;
                        }

                        list.add(0,timeTask);
                    }




                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }catch (JSONException e) {
                    e.printStackTrace();
                }finally
                {
                    urlConnection.disconnect();
                }


                return null;
            }


            @Override
            protected void onPostExecute(Void aVoid) {

                if(time_running)
                {
                    fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_stop_24dp, getApplicationContext().getTheme()));
                    fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                }
                else
                {
                    fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_arrow_24dp, getApplicationContext().getTheme()));
                    fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                }
                arrayAdapter.notifyDataSetChanged();
                super.onPostExecute(aVoid);
            }
        };

        task.execute();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
